---
title: "Random Access Memories"
date: 2023-07-22T06:37:31-03:00
draft: false
---

# 「　Wired Memories for Wired People　」

This is a random collection of a wide range of topics related to programming,
devops, Linux, GNU, FLOSS (Free/Libre, and Open Source Software), music, anime,
sci-fi, cyberpunk, and technology.

__Welcome, to the Machine!__
