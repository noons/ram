# sheru the _shell-like superstylin'_ hugo theme

> This is still an early and incomplete version

I have designed this is as an example of minimalism, resembling a shell.
It has been crafted with an emphasis on simplicity, offering an uncluttered and sleek aesthetic, With a focus on efficiency, utilizing the bare minimum of Javascript, resulting in fasts load times and improved performance.

## Awesome projects used to build this theme:

-   [momonoki](https://madmalik.github.io/mononoki/)
-   [terminal.css](https://github.com/Gioni06/terminal.css)
-   [woff2-tools](https://github.com/google/woff2)

## Requirements:

-   [hugo](https://gohugo.io/) `>= 0.111.3`

## To-Do:

-   [ ] Add Grid System (maybe [bootstrap-grid](https://getbootstrap.com/docs/5.3/getting-started/download/))
-   [ ] Add copy to clipboard button
-   [ ] Add mobile enhancement
-   [ ] Add Categories
-   [ ] Add multi-language support
-   [ ] Add rss/atom


## About

Author: [noons](https://noons.codeberg.page/ram)

License: [MIT](./LICENSE)
