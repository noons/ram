# 「　Wired Memories for Wired People　」

This is a random collection of a wide range of topics related to programming,
devops, Linux, GNU, FLOSS (Free/Libre, and Open Source Software), music, anime,
sci-fi, cyberpunk, and technology.

__Welcome, to the Machine!__

## About

Author: [noons](https://noons.codeberg.page/ram)

License: [CC BY-SA 4.0](./LICENSE)
